import random
from threading import Lock, Thread, Event
import time
import asyncio
from queue import PriorityQueue, LifoQueue, Queue

class Process(Thread):
    id_n = 1
    rlock = Lock()
    wlock = Lock()
    _idLength = 3
    procList = []

    def __init__(self, q):
        super(Process,self).__init__()
        self.q = q #Global queue
        self.proc_q = PriorityQueue() #Private queue
        #self.id = self.randomID()
        self.id = Process.id_n
        Process.id_n += 1
        self.active = True #true by default

        self.stoprequest = Event()
        self.receivedAnswer = False

    def deactivate(self):
        (self.id, ":> deactivated")
        self.active = False
        return


    def sendMessage(self, msg, proc):
        proc.proc_q.put((100-self.id, msg, self))
        return

    def startElection(self):
        for proc in self.procList:
            if proc.id > self.id:
                self.sendMessage("ELECT", proc)
                #print(self.id, ":>", "Sent ELECT to", proc.id)
        return


    def run(self):
        #print("Process -> ", self.id, "created")
        while not self.stoprequest.isSet():
            if not self.proc_q.empty() and self.active:
                message = self.proc_q.get()
                if message[1] == "OK":
                    self.wlock.acquire()
                    self.receivedAnswer = True
                    self.wlock.release()
                    print(self.id, ":> received ", message[1], "from", message[2].id)

                if not self.receivedAnswer and message[1] == "ELECT":
                    print(self.id, ":> received ", message[1], "from", message[2].id)
                    self.sendMessage("OK", message[2])
                    self.startElection()
                ###Wait for responses

                #if message[1] == "COORDINATOR":
                #    print(self.id, ":> acknowledged", message[2].id, "as new coordinator!")
                #    self.wait()

                time.sleep(.5)

                if not self.receivedAnswer and self.proc_q.empty():
                    print("New coordinator elected -> ", self.id)
                #    for proc in self.procList:
                #        self.sendMessage("COORDINATOR", proc)
                #        self.receivedAnswer = True;
                #    self.wait()





    def __str__(self):
        string = "Process ID -> "
        string += str(self.id)
        return(string)
    ## Gives the process a list of all other process IDS
    def tell(self,proc):
        self.procList = proc

    def isActive(self):
       print(self.id, "-> ", "active" if (self.active > 0) else "inactive")
    #    print("x")
    def join(self, timeout=None):
        self.stoprequest.set()
        super(Process, self).join(timeout)

    def randomID(self):
        range_start = 10**(self._idLength-1)
        range_end = (10**self._idLength)-1
        return random.randint(range_start, range_end)
