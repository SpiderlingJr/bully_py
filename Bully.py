import threading
import time
#import sys
import sys
from queue import Queue

from Process import Process #custom Process class from Process.py

NO_OF_PROCS = 10
threads = []
q = Queue()
#word = "helloguys"
#print("PROOF THAT THIS IS MULTITHREADED AND ASYNCHRONOUS")
#print("trying to print \"helloguys\"")
#for x in range(len(word)):
#    q.put(word[x])

def setup_procs():
    for proc in range(NO_OF_PROCS):
        P = Process(q)
        threads.append(P)
        P.start()
    return
def tell_procs():
    for thread in threads:
        thread.tell(threads)

def print_table():
    clear = "\n" * 10
    print(clear)

    print()
    print("|------------|--------------|-------------|")
    print("|   NAME     |      ID      |    ACTIVE   |")
    print("|------------|--------------|-------------|")
    for thread in threads:
        print("| ",thread.name," |\t ",thread.id,"    |   ",thread.active,"    |")
    print("|------------|--------------|-------------|")
    return



def print_menu():
    quit_menu = False
    while not quit_menu:
        deadThread = input("Select Thread to deactivate: ")
        initiator = input("Select Thread to initiate election: ")
        #notifyAll()

        try:
            if deadThread == initiator:
                print("Deactivated Thread cannot be initiator")
            else:
                deadThread = int(deadThread)
                initiator = int(initiator)
                for thread in threads:
                    thread.receivedAnswer = False

                print("Deactivate Thread", deadThread)
                threads[deadThread-1].deactivate()
                threads[initiator-1].startElection()
                print()

                    ### insert bully messaging here

        except:
            if  deadThread == 'quit':
                print("Quits")
                quit_menu = True
            else:
                continue
    return

#Init Threads
setup_procs()
#Tell all threads who else is at the party
tell_procs()
#Print them
time.sleep(.1)
print_table()

print_menu()

#time.sleep(0.5)
#print_table()
#processes[0].active()
#q.join()

print()
print("Join -> ", end=" ")
for thread in threads:
    print(thread.id, end=" ")
    thread.join()
